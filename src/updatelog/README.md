---
# 当前页面内容标题
title: 网站更新日志
# 当前页面图标
icon: rizhi

# 是否将该页面生成在侧边栏
index: false

sticky: false
# 是否收藏在博客主题的文章列表中，当填入数字时，数字越大，排名越靠前。
star: false
# 是否将该文章添加至文章列表中
article: false
# 是否将该文章添加至时间线中
timeline: false
---


## 2023 年 10 月
::: tip 新增项目作品
- [项目](../project/)：[Wolai2Notion](https://github.com/AruNi-01/wolai2notion)

:::


## 2023 年 8 月
::: tip 新增文章

**《计算机基础》**：
- 操作系统：[阻塞非阻塞同步异步 IO](../studynotes/cs/os/net_system/阻塞非阻塞同步异步IO.md)

**《Java》**：
- JVM：[逃逸分析](../studynotes/java/jvm/compile_and_optimize/逃逸分析.md)
- JVM：[运行时数据区域](../studynotes/java/jvm/memory_manager/运行时数据区域.md)
- JVM：[对象是如何创建出来的](../studynotes/java/jvm/memory_manager/对象是如何创建出来的.md)
- JVM：[GC｜对象存活与否](../studynotes/java/jvm/memory_manager/GC｜对象存活与否.md)

:::


## 2023 年 7 月
::: tip 新增文章

**《Java》**：
- 并发编程：[手撕简易线程池](../studynotes/java/concurrency/手撕简易线程池.md)

**《数据库》**：
- MySQL：[行锁的加锁规则](../studynotes/database/mysql/lock/行锁的加锁规则.md)


**《微服务架构》**：
- 负载均衡：[常见负载均衡算法](../studynotes/microservice/load_balance/常见负载均衡算法.md)
- 服务熔断限流与降级：[熔断 - 如何防止抖动](../studynotes/microservice/fuse_limit_downgrade/熔断-如何防止抖动.md)


**《设计模式》**：
- 设计模式与范式：[模板模式（上）：理解模板模式](../studynotes/design_pattern/pattern/behaviour_type/模板模式（上）：理解模板模式.md)
- 设计模式与范式：[模板模式（下）：模板模式与 Callback](../studynotes/design_pattern/pattern/behaviour_type/模板模版（下）：模板模式与Callback.md)

:::

::: info 完善文章
**《Java》**：
- 并发编程：[volatile 详解](../studynotes/java/concurrency/volatile详解.md)

**《数据库》**：
- MySQL：[MySQL 中的锁](../studynotes/database/mysql/lock/MySQL中的锁.md)

:::

## 2023 年 6 月
::: tip 新增文章
**《分布式系统》**：
- 基础：[什么是分布式](../studynotes/distributed/basis/什么是分布式.md)
- 基础：[分布式系统的指标](../studynotes/distributed/basis/分布式系统的指标.md)
- 分布式协调与同步：[分布式互斥：一山不容二虎](../studynotes/distributed/coord_and_sync/分布式互斥：一山不容二虎.md)
- 分布式协调与同步：[分布式选举：国不可一日无君](../studynotes/distributed/coord_and_sync/分布式选举：国不可一日无君.md)

**《微服务架构》**：
- 基础：[什么是微服务](../studynotes/microservice/basis/什么是微服务.md)
- 基础：[服务如何拆分](../studynotes/microservice/basis/服务如何拆分.md)
- 基础：[初探微服务架构](../studynotes/microservice/basis/初探微服务架构.md)
- 服务注册与发现：[什么是服务注册与发现](../studynotes/microservice/register_and_discover/什么是服务注册与发现.md)


**《计算机基础》**：
- 操作系统：[如何让程序跑的更快](../studynotes/cs/os/basis/如何让程序跑的更快.md)

**《设计模式》**：
- 设计模式与范式：[观察者模式（上）：理解观察者模式](../studynotes/design_pattern/pattern/behaviour_type/观察者模式（上）：理解观察者模式.md)
- 设计模式与范式：[观察者模式（下）：实现一个 EventBus 框架](../studynotes/design_pattern/pattern/behaviour_type/观察者模式（下）：实现一个EventBus框架.md)

**《中间件》**：
- 消息队列：[MQ 常见问题合集](../studynotes/middleware/mq/common_question/MQ常见问题合集.md)
- 消息队列：[如何保证消息幂等](../studynotes/middleware/mq/common_question/如何保证消息幂等.md)

:::


::: info 完善文章
**《设计模式》**：
- 设计模式与范式：[装饰器模式](../studynotes/design_pattern/pattern/structure_type/装饰器模式.md)

:::


## 2023 年 4 月
::: tip 新增文章
**《设计模式》**：
- 设计模式与范式：[工厂模式](../studynotes/design_pattern/pattern/create_type/工厂模式.md)
- 设计模式与范式：[建造者模式](../studynotes/design_pattern/pattern/create_type/建造者模式.md)
- 设计模式与范式：[原型模式](../studynotes/design_pattern/pattern/create_type/原型模式.md)
- 设计模式与范式：[代理模式](../studynotes/design_pattern/pattern/structure_type/代理模式.md)
- 设计模式与范式：[桥接模式](../studynotes/design_pattern/pattern/structure_type/桥接模式.md)
- 设计模式与范式：[装饰器模式](../studynotes/design_pattern/pattern/structure_type/装饰器模式.md)
- 设计模式与范式：[适配器模式](../studynotes/design_pattern/pattern/structure_type/适配器模式.md)
- 设计模式与范式：[门面模式](../studynotes/design_pattern/pattern/structure_type/门面模式.md)
- 设计模式与范式：[组合模式](../studynotes/design_pattern/pattern/structure_type/组合模式.md)
- 设计模式与范式：[享元模式](../studynotes/design_pattern/pattern/structure_type/享元模式.md)

:::

::: info 完善文章
**《Java》**：
- Java 基础：[String 类](../studynotes/java/javase/String类.md)

:::

## 2023 年 3 月

::: tip 新增文章
**《数据库》**：
- MySQL：[了解 Buffer Pool](../studynotes/database/mysql/buffer_pool/了解BufferPool.md)
- MySQL：[提高缓存命中率的 LRU 链表](../studynotes/database/mysql/buffer_pool/提高缓存命中率的LRU链表.md)
- MySQL：[redo log：崩溃恢复神器](../studynotes/database/mysql/log/redo%20log：崩溃恢复神器.md)
- MySQL：[binlog：主从复制和备份](../studynotes/database/mysql/log/binlog：主从复制和备份.md)
- MySQL：[update 执行流程](../studynotes/database/mysql/log/update%20执行流程.md)
- MySQL：[两阶段提交有什么问题](../studynotes/database/mysql/log/两阶段提交有什么问题.md)
- MySQL：[undo log：世上真有后悔药](../studynotes/database/mysql/log/undo%20log：世上真有后悔药.md)


**《计算机基础》**：
- 网络：[计算机网络模型](../studynotes/cs/network/basis/计算机网络模型.md)
- 网络：[键入 URL 到页面显示全过程](../studynotes/cs/network/basis/键入URL到页面显示全过程.md)


**《框架》**：
- small-spring：[第01章：简单的 Bean 容器](../studynotes/framework/small-spring/ioc/第01章：简单的Bean容器.md)
- small-spring：[第02章：Bean 的定义、注册、获取](../studynotes/framework/small-spring/ioc/第02章：Bean%20的定义、注册、获取.md)
- small-spring：[第03章：实现含构造函数的类实例化策略](../studynotes/framework/small-spring/ioc/第03章：实现含构造函数的类实例化策略.md)
- small-spring：[第04章：注入属性和依赖对象](../studynotes/framework/small-spring/ioc/第04章：注入属性和依赖对象.md)
- small-spring：[第05章：资源加载器解析文件注册对象](../studynotes/framework/small-spring/ioc/第05章：资源加载器解析文件注册对象.md)
- small-spring：[第06章：实现应用上下文](../studynotes/framework/small-spring/ioc/第06章：实现应用上下文.md)
- small-spring：[第07章：初始化和销毁方法](../studynotes/framework/small-spring/ioc/第07章：初始化和销毁方法.md)
- small-spring：[第08章：Aware 感知容器对象](../studynotes/framework/small-spring/ioc/第08章：Aware%20感知容器对象.md)
- small-spring：[第09章：对象作用域和 FactoryBean](../studynotes/framework/small-spring/ioc/第09章：对象作用域和%20FactoryBean.md)
- small-spring：[第10章：容器事件和事件监听器](../studynotes/framework/small-spring/ioc/第10章：容器事件和事件监听器.md)


**《设计模式》**：
- 设计原则与思想：[面向对象是什么](../studynotes/design_pattern/mind/oop/面向对象是什么.md)
- 设计原则与思想：[面向对象和面向过程的区别](../studynotes/design_pattern/mind/oop/面向对象和面向过程的区别.md)
- 设计原则与思想：[你写的真的是面向对象的代码吗](../studynotes/design_pattern/mind/oop/你写的真的是面向对象的代码吗.md)
- 设计原则与思想：[真正理解接口和抽象类](../studynotes/design_pattern/mind/oop/真正理解接口和抽象类.md)
- 设计原则与思想：[理论：传统 MVC vs DDD](../studynotes/design_pattern/mind/oop/理论：传统%20MVC%20vs%20DDD.md)
- 设计原则与思想：[实战：传统 MVC vs DDD](../studynotes/design_pattern/mind/oop/实战：传统%20MVC%20vs%20DDD.md)
- 设计原则与思想：[面向对象开发实战](../studynotes/design_pattern/mind/oop/面向对象开发实战.md)
- 设计原则与思想：[常见设计原则](../studynotes/design_pattern/mind/design_principle/常见设计原则.md)
- 设计原则与思想：[设计原则补充](../studynotes/design_pattern/mind/design_principle/设计原则补充.md)
- 设计模式与范式：[单例模式](../studynotes/design_pattern/pattern/create_type/单例模式.md)

:::

## 2023 年 2 月

::: tip 新增文章
**《Java》**：
- 集合：[集合入门](../studynotes/java/collection/集合入门.md)
- 集合：[ArrayList 源码分析](../studynotes/java/collection/ArrayList源码分析.md)
- 集合：[HashMap 源码分析：数据结构](../studynotes/java/collection/HashMap源码分析：数据结构.md)
- 集合：[HashMap 源码分析：功能实现](../studynotes/java/collection/HashMap源码分析：功能实现.md)
- 并发编程：[AQS 入门：简单了解](../studynotes/java/concurrency/AQS入门：简单了解.md)

**《数据库》**：
- MySQL：[MySQL 常见存储引擎](../studynotes/database/mysql/basis/MySQL常见存储引擎.md)
- MySQL：[select 执行流程](../studynotes/database/mysql/basis/select执行流程.md)
- MySQL：[执行计划之 explain](../studynotes/database/mysql/index/执行计划之explain.md)
- MySQL：[索引覆盖和索引条件下推](../studynotes/database/mysql/index/索引覆盖和索引条件下推.md)
- MySQL：[联合索引与最左前缀匹配](../studynotes/database/mysql/index/联合索引与最左前缀匹配.md)
- MySQL：[MySQL 中的锁](../studynotes/database/mysql/lock/MySQL中的锁.md)
- Redis：[kv 数据库如何实现](../studynotes/database/redis/basis/kv数据库如何实现.md)

**《计算机基础》**：
- 网络：[HTTPS 入门](../studynotes/cs/network/http/HTTPS入门.md)
:::


## 2022 年 12 月

::: tip 新增文章
**《Java》**：
- Java 基础：[Object 类](../studynotes/java/javase/Object类.md)
- Java 基础：[String 类](../studynotes/java/javase/String类.md)
- 并发编程：[ThreadLocal 详解](../studynotes/java/concurrency/ThreadLocal详解.md)
- 并发编程：[Java 内存模型](../studynotes/java/concurrency/Java内存模型.md)
- 并发编程：[volatile 详解](../studynotes/java/concurrency/volatile详解.md)

**《计算机基础》**：
- 网络：[HTTP 入门](../studynotes/cs/network/http/HTTP入门.md)

:::
