---
home: true
icon: home2
title: 个人主页
heroImage: /favicon.png
heroText: Code Tracks Box
tagline: 🌱 人生最后悔的事情就是，我本可以
actions:
  - text: 开始阅读 🎉
    link: /studynotes/
    type: primary
  - text: 时间轴 🗓️
    link: /timeline/

features:
  - title: Java
    icon: java
    details: Java 基础、集合、并发编程
    link: /studynotes/java/
  - title: 数据库
    icon: database
    details: 数据库知识、MySQL、Redis
    link: /studynotes/database/
  - title: 计算机基础
    icon: computer
    details: 操作系统、网络相关
    link: /studynotes/cs/
  - title: 设计模式
    icon: design-pattern
    details: 设计模式与范式相关
    link: /studynotes/design_pattern/
  - title: 分布式系统
    icon: fenbushi
    details: 分布式技术基础及原理
    link: /studynotes/distributed/
  - title: 微服务架构
    icon: microservice
    details: 微服务体系的基本原理和组成
    link: /studynotes/microservice/
  - title: 中间件
    icon: middleware
    details: 后端常用中间件
    link: /studynotes/middleware/
  - title: 框架
    icon: framework
    details: 后端常用框架
    link: /studynotes/framework/

copyright: false
footer: Copyright © 2022-2023 AruNi_Lu
---

## 📣 About
> 👦🏻 **Me**：一名后端开发，目前就读于武汉工程大学，软件工程专业大三

> 🌀 **Website**：我的代码足迹盒，以此来记录一路走来的学习足迹、随感随想等

## 👋🏻 Contact
> 🐧 **QQ**：<a href="tencent://AddContact/?fromId=50&fromSubId=1&subcmd=all&uin=1298911600">Click Me</a>

> 🎗️ **Wechat**：
> 
> ![wx](/wx.jpg)

## 🌈 Thanks
> ⚡ Power By <a href="https://v2.vuepress.vuejs.org/zh/" target="_blank">VuePress</a>

> 🎨 Theme By <a href="https://theme-hope.vuejs.press/zh/" target="_blank">VuePress-Hope-Theme</a>

> 🚀 Deploy By <a href="https://docs.gitlab.com/ee/user/project/pages/" target="_blank">GitLab Pages</a> & <a href="https://vercel.com/" target="_blank">Vercel</a>

> 🌟 Rapid deployment of Vuepress using Vercel：[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/clone?demo-title=Vuepress&demo-description=Vue-powered%20Static%20Site%20Generator&demo-url=https%3A%2F%2Fvuepress-starter-template.vercel.app%2F&demo-image=%2F%2Fimages.ctfassets.net%2Fe5382hct74si%2F1umwsLFT0iuxtmqqVQvV26%2Fba666c531fe100a30c72f2d638193f05%2F678f323f-23b8-44c1-b65f-8dad78ba083c.png&project-name=Vuepress&repository-name=vuepress&repository-url=https%3A%2F%2Fgithub.com%2Fvercel%2Fvercel%2Ftree%2Fmain%2Fexamples%2Fvuepress&from=templates&skippable-integrations=1)

---

<br>

<div style="text-align: center;">
<img src="https://therealsujitk-vercel-badge.vercel.app/?app=aruni"> &nbsp
<img src="https://gitlab.com/aaryn/aaryn.gitlab.io/badges/main/pipeline.svg"> &nbsp
<img src="https://api.netlify.com/api/v1/badges/bd9531da-c2d4-4c8b-850b-e56d91b1e251/deploy-status"> &nbsp
<img src="https://gitlab.com/aaryn/aaryn.gitlab.io/-/badges/release.svg"> &nbsp
</div>
