---
title: 微服务架构
date: 2023-06-11
icon: microservice
index: false
dir:
  order: 6

# 分类
category:
 - 导航
# 标签
tag:
 - 导航

sticky: false
# 是否收藏在博客主题的文章列表中，当填入数字时，数字越大，排名越靠前。
star: false
# 是否将该文章添加至文章列表中
article: false
# 是否将该文章添加至时间线中
timeline: false
---

::: tip 相关内容
微服务架构，有关服务注册与发现、远程调用、网关、负载均衡、服务熔断、降级等知识！

::: right
——参考：胡忠想《从 0 开始学微服务》

:::

## 基础
- [什么是微服务](basis/什么是微服务.md)
- [服务如何拆分](basis/服务如何拆分.md)
- [初探微服务架构](basis/初探微服务架构.md)

## 服务注册与发现
- [什么是服务注册与发现](register_and_discover/什么是服务注册与发现.md)

## 负载均衡
- [常见负载均衡算法](load_balance/常见负载均衡算法.md)

## 服务熔断限流与降级
- [熔断 - 如何防止抖动](fuse_limit_downgrade/熔断-如何防止抖动.md)
