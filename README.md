# AruNi's docs

[![Static Badge](https://img.shields.io/badge/Website-AruNi's_Domain-%2340b883)](https://aruni.me/) [![Static Badge](https://img.shields.io/badge/GitLab-Aaryn-%23fc6d26)](https://gitlab.com/aaryn/aaryn.gitlab.io) [![Static Badge](https://img.shields.io/badge/GitHub-AruNi-%23202328)](https://github.com/AruNi-01/AruNi-01.github.io)

[![Netlify Status](https://api.netlify.com/api/v1/badges/bd9531da-c2d4-4c8b-850b-e56d91b1e251/deploy-status)](https://app.netlify.com/sites/aruni/deploys)
[![Vercel](https://therealsujitk-vercel-badge.vercel.app/?app=aruni)](https://vercel.com/aarynlu/aruni)
[![Gitlab Pipeline](https://gitlab.com/aaryn/aaryn.gitlab.io/badges/main/pipeline.svg)](https://gitlab.com/aaryn/aaryn.gitlab.io/-/pipelines)
[![Gitlab Release](https://gitlab.com/aaryn/aaryn.gitlab.io/-/badges/release.svg)](https://gitlab.com/aaryn/aaryn.gitlab.io/-/releases)


## 🌈 Thanks
> ⚡ Power By <a href="https://v2.vuepress.vuejs.org/zh/" target="_blank">VuePress</a>

> 🎨 Theme By <a href="https://theme-hope.vuejs.press/zh/" target="_blank">VuePress-Hope-Theme</a>

> 🚀 Deploy By <a href="https://docs.gitlab.com/ee/user/project/pages/" target="_blank">GitLab Pages</a> & <a href="https://vercel.com/" target="_blank">Vercel</a>

Rapid deployment of Vuepress using Vercel：
[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/clone?demo-title=Vuepress&demo-description=Vue-powered%20Static%20Site%20Generator&demo-url=https%3A%2F%2Fvuepress-starter-template.vercel.app%2F&demo-image=%2F%2Fimages.ctfassets.net%2Fe5382hct74si%2F1umwsLFT0iuxtmqqVQvV26%2Fba666c531fe100a30c72f2d638193f05%2F678f323f-23b8-44c1-b65f-8dad78ba083c.png&project-name=Vuepress&repository-name=vuepress&repository-url=https%3A%2F%2Fgithub.com%2Fvercel%2Fvercel%2Ftree%2Fmain%2Fexamples%2Fvuepress&from=templates&skippable-integrations=1)

